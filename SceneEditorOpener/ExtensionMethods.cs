﻿using System.ComponentModel;

public static partial class ExtensionMethods
{
    public static string ToDescription(this Scenes value)
    {
        DescriptionAttribute[] da = (DescriptionAttribute[]) value.GetType()
            .GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
        return da.Length > 0 ? da[0].Description : value.ToString();
    }
}