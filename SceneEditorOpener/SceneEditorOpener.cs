﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class SceneEditorOpener : MonoBehaviour
{
    [MenuItem("Scenes/01 - Scene01")]
    private static void OpenScene01()
    {
        OpenScene(Scenes.Scene01.ToDescription());
    }

    private static void OpenScene(string sceneName)
    {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene($"Assets/Scenes/{sceneName}.unity");
    }
}